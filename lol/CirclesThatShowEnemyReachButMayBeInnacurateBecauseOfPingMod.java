package lol;

import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import java.util.Iterator;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.Entity;
import org.lwjgl.opengl.GL11;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraft.command.ICommand;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.Mod;

@Mod(modid = "range circle mod lol", version = "1.0")
public class CirclesThatShowEnemyReachButMayBeInnacurateBecauseOfPingMod
{
    public static Minecraft mc;
    public static boolean show;
    
    @Mod.EventHandler
    public void init(final FMLInitializationEvent event) {
        FMLCommonHandler.instance().bus().register((Object)this);
        MinecraftForge.EVENT_BUS.register((Object)this);
        ClientCommandHandler.instance.registerCommand((ICommand)new CommandThatTogglesTheCircles());
    }
    
    @SubscribeEvent
    public void onRender(final RenderWorldLastEvent event) {
        GL11.glPushMatrix();
        GL11.glDisable(3553);
        GL11.glEnable(3042);
        GL11.glBlendFunc(770, 771);
        GL11.glDisable(2929);
        GL11.glEnable(2848);
        GL11.glDepthMask(false);

        for (final Object o : CirclesThatShowEnemyReachButMayBeInnacurateBecauseOfPingMod.mc.theWorld.loadedEntityList) {
            final Entity entity = (Entity)o;
            if (entity instanceof EntityLivingBase && !entity.isInvisible() && entity != CirclesThatShowEnemyReachButMayBeInnacurateBecauseOfPingMod.mc.thePlayer && CirclesThatShowEnemyReachButMayBeInnacurateBecauseOfPingMod.show && entity.isEntityAlive() && ((EntityLivingBase)entity).canEntityBeSeen((Entity)CirclesThatShowEnemyReachButMayBeInnacurateBecauseOfPingMod.mc.thePlayer)) {
                final double posX = entity.lastTickPosX + (entity.posX - entity.lastTickPosX) * event.getPartialTicks() - Minecraft.getMinecraft().getRenderManager().viewerPosX;
                final double posY = entity.lastTickPosY + (entity.posY - entity.lastTickPosY) * event.getPartialTicks() - Minecraft.getMinecraft().getRenderManager().viewerPosY;
                final double posZ = entity.lastTickPosZ + (entity.posZ - entity.lastTickPosZ) * event.getPartialTicks() - Minecraft.getMinecraft().getRenderManager().viewerPosZ;
                this.circle(posX, posY, posZ, CirclesThatShowEnemyReachButMayBeInnacurateBecauseOfPingMod.mc.playerController.isInCreativeMode() ? 5.3 : 3.3, entity);
            }
        }
        GL11.glDepthMask(true);
        GL11.glDisable(2848);
        GL11.glEnable(2929);
        GL11.glDisable(3042);
        GL11.glEnable(3553);

        GL11.glPopMatrix();
    }
    
    private void circle(final double x, final double y, final double z, final double rad, Entity e) {
        GL11.glPushMatrix();
        final double tau = 6.283185307179586;
        final double fans = 45.0;
        GL11.glLineWidth(.9f);
        GL11.glBegin(1);
        if(e.getDistanceToEntity(mc.thePlayer) <= 3.3)
            GL11.glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
        else
            GL11.glColor4f(1.0f, 0.0f, 0.0f, 1.0f);

        for (int i = 0; i <= 90; ++i) {
            GL11.glVertex3d(x + rad * Math.cos(i * tau / fans), y, z + rad * Math.sin(i * tau / fans));
        }

        GL11.glEnd();
        GL11.glPopMatrix();
    }
    
    static {
        CirclesThatShowEnemyReachButMayBeInnacurateBecauseOfPingMod.mc = Minecraft.getMinecraft();
        CirclesThatShowEnemyReachButMayBeInnacurateBecauseOfPingMod.show = false;
    }
}

package lol;

import net.minecraft.client.Minecraft;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.CommandBase;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;

public class CommandThatTogglesTheCircles extends CommandBase
{
    public String getCommandName() {
        return "circles";
    }
    
    public String getCommandUsage(final ICommandSender sender) {
        return "";
    }

    @Override
    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
        CirclesThatShowEnemyReachButMayBeInnacurateBecauseOfPingMod.show = !CirclesThatShowEnemyReachButMayBeInnacurateBecauseOfPingMod.show;
    }

    public int getRequiredPermissionLevel() {
        return 0;
    }
    
    public boolean canCommandSenderUseCommand(final ICommandSender sender) {
        return true;
    }
}
